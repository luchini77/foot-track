(function (){
    const btn_eliminacion = document.querySelectorAll(".btn_eliminacion");

    btn_eliminacion.forEach(btn=>{
        btn.addEventListener('click',(e)=>{
            const confirmacion = confirm('¿Seguro de eliminar el curso?');
            if(!confirmacion){
                e.preventDefault();
            }
        });
    });
})();