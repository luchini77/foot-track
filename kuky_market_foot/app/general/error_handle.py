from flask import render_template

from app import app


@app.errorhandler(404)
def pagina_no_encontrada(e):
    return render_template("error/404.html")