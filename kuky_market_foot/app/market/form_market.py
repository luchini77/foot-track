from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField, HiddenField
from wtforms.validators import InputRequired, Length, NumberRange


class ProductoForm(FlaskForm):
    nombre = StringField('Nombre', validators=[InputRequired(), Length(min=4, max=20)])
    descripcion = StringField('Descripcion', validators=[InputRequired(), Length(min=1, max=120)])
    precio = IntegerField('Precio', validators=[InputRequired()])
    categoria_id = StringField('Categoria', validators=[InputRequired(), Length(min=4, max=20)])
    submit = SubmitField('Enviar')


class CategoriaForm(FlaskForm):
    nombre = StringField('Nombre', validators=[InputRequired(), Length(min=1, max=20)])
    submit = SubmitField('Enviar')


class CarritoForm(FlaskForm):
    id = HiddenField('id')
    cantidad = IntegerField('Cantidad', validators=[InputRequired(), NumberRange(min=1)])
    submit = SubmitField('Guardar')