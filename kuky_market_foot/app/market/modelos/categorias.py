from app import db


class Categoria(db.Model):
    __tablename__ = 'categorias'
    id = db.Column(db.Integer, primary_key=True)
    categoria = db.Column(db.String(30), nullable=False)
    productos = db.relationship('Producto', backref='categorias', passive_deletes=True)


    def __init__(self, categoria):
        self.categoria = categoria


    def __repr__(self):
        return f"Nombre de la Categoria: {self.categoria}"
