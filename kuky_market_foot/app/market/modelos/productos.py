from app import db


class Producto(db.Model):
    __tablename__ = 'productos'
    id = db.Column(db.Integer, primary_key=True)
    nombre_producto = db.Column(db.String(128), nullable=False)
    descripcion = db.Column(db.String(250), nullable=False)
    precio_venta = db.Column(db.Integer, nullable=False)
    categoria_id = db.Column(db.Integer, db.ForeignKey('categorias.id', ondelete='CASCADE'), nullable=False)


    def __init__(self, nombre_producto, descripcion, precio_venta, categoria_id):
        self.nombre_producto = nombre_producto
        self.descripcion = descripcion
        self.precio_venta = precio_venta
        self.categoria_id = categoria_id



    def __repr__(self):
        return f"Nombre: {self.nombre_producto}"