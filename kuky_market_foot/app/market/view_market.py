from flask import render_template, Blueprint, flash, redirect, url_for, request
from flask_login import login_required, current_user

from .modelos.productos import Producto
from .modelos.categorias import Categoria
from .form_market import ProductoForm, CategoriaForm
from app import app, db



market = Blueprint('market', __name__, url_prefix="/market/")


#PAGINA PRINCIPAL
@app.route('/')
def index():
    categorias = Categoria.query.all()

    contex = {
        'usuario':current_user,
        'categorias':categorias
    }

    return render_template('index.html', **contex)

@market.route('/productos')
@market.route('/productos/<int:id>')
@login_required
def productos(id=0):
    categoria = Categoria.query.get(id)
    if id == 0:
        productos = Producto.query.all()
    else:
        productos = Producto.query.filter_by(categoria_id=id)
    categorias = Categoria.query.all()
  
    return render_template('market/productos.html',categoria=categoria,productos=productos, categorias=categorias)


@market.route('/lista_productos', methods=['GET', 'POST'])
@login_required
def lista_productos():
    categorias = Categoria.query.all()

    productos = Producto.query.all()
    return render_template('market/lista_productos.html', productos=productos, categorias=categorias)


@market.route('/nuevo_producto', methods=['GET', 'POST'])
@login_required
def nuevo_producto():
    form = ProductoForm(request.form)

    if request.method == 'POST':
        producto = Producto(nombre_producto=form.nombre.data, descripcion=form.descripcion.data, precio_venta=form.precio.data, categoria_id=request.form.get('categoria'))
        db.session.add(producto)
        db.session.commit()
        flash('Producto creado con exito!')
        return redirect(url_for('market.lista_productos'))

    return render_template('market/lista_productos.html', form=form)


@market.route('/actualizar_productos/<int:id>', methods=['GET', 'POST'])
@login_required
def actualizar_productos(id):
    producto = Producto.query.get(id)
    categorias = Categoria.query.all()
    form = ProductoForm(request.form)
    if request.method == 'POST':
        producto.nombre_producto = request.form['nombre']
        producto.descripcion = request.form['descripcion']
        producto.precio_venta = request.form['precio']
        producto.categoria_id = request.form.get('categoria')
        try:
            db.session.commit()
            flash('Producto actualizado con exito!')
            return redirect(url_for('market.lista_productos'))
        except:
            return 'Error al actualizar el producto'

    return render_template('market/actualizar_productos.html', form=form, producto=producto, categorias=categorias)


@market.route('/eliminar_producto/<int:id>', methods=['GET', 'POST'])
@login_required
def eliminar_producto(id):
    producto = Producto.query.get(id)
    try:
        db.session.delete(producto)
        db.session.commit()
        flash('Producto eliminado con exito!')
        return redirect(url_for('market.lista_productos'))
    except:
        return 'Error al eliminar el producto'


@market.route('/categoria')
@login_required
def categoria():
    categorias = Categoria.query.all()
    return render_template('market/categoria.html', categorias=categorias)


@market.route('/agregar_categorias', methods=['GET', 'POST'])
@login_required
def agregar_categorias():
    form = CategoriaForm()

    if form.validate_on_submit():
        categoria = Categoria(categoria=form.nombre.data)
        db.session.add(categoria)
        db.session.commit()
        flash('Categoria agregada exitosamente!')
        return redirect(url_for('market.categoria'))

    return render_template('market/categoria.html', form=form)


@market.route('/actualizar_categorias/<int:id>', methods=['GET', 'POST'])
@login_required
def actualizar_categorias(id):
    cat_updt = Categoria.query.get(id)
    
    if request.method == 'POST':
        cat_updt.categoria = request.form['nombre']
        try:
            db.session.commit()
            flash('Categoria actualizada exitosamente!')
            return redirect(url_for('market.categoria'))
        except:
            return 'Error al actualizar la categoria'

    return render_template('market/actualizar_categoria.html', categoria=cat_updt)


@market.route('/eliminar_categorias/<int:id>', methods=['GET', 'POST'])
@login_required
def eliminar_categorias(id):
    cat_del = Categoria.query.get(id)
    try:
        db.session.delete(cat_del)
        db.session.commit()
        flash('Categoria eliminada exitosamente!')
        return redirect(url_for('market.categoria'))
    except:
        return 'Error al eliminar la categoria'