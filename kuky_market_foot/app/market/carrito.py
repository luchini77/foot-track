from flask import Blueprint, make_response, render_template, flash, redirect, url_for, request
from flask_login import login_required, current_user
import json

from .modelos.productos import Producto
from .form_market import CarritoForm


carro = Blueprint('carro', __name__, url_prefix="/carrito/")


@carro.route('/agregar_carrito/<int:id>', methods=['GET', 'POST'])
@login_required
def agregar_carrito(id):
    stock = 50
    producto = Producto.query.get(id)

    form = CarritoForm()
    form.id.data = id

    if form.validate_on_submit():
        if stock >= int(form.cantidad.data):
            try:
                datos = json.loads(request.cookies.get(str(current_user.id)))
            except:
                datos = []

            actualizar = False
            for dato in datos:
                if dato['id'] == id:
                    dato['precio'] = Producto.query.get(id).precio_venta
                    dato['cantidad'] = form.cantidad.data
                    actualizar = True
            if not actualizar:
                datos.append({'id': form.id.data,'precio':producto.precio_venta, 'cantidad': form.cantidad.data})

            resp = make_response(redirect(url_for('carro.carrito')))
            resp.set_cookie(str(current_user.id), json.dumps(datos))
            return resp

        form.cantidad.errors.append('Stock insuficiente')

    return render_template('carrito/agregar_carrito.html', form=form, producto=producto)


@carro.route('/')
@login_required
def carrito():

    try:
        datos = json.loads(request.cookies.get(str(current_user.id)))
    except:
        datos = []
    
    if not datos:
        return redirect(url_for('market.productos'))
    else:

        productos = []
        precios = []
        cantidades = []
        total = 0

        for producto in datos:

            productos.append(Producto.query.get(producto['id']))
            precios.append(producto.get('precio'))
            cantidades.append(producto['cantidad'])

            total = total + (producto.get('precio') * producto['cantidad'])
            
        productos = zip(productos,precios, cantidades)

    return render_template('carrito/carrito.html', productos=productos, total=total)


@carro.route('/eliminar_producto/<int:id>')
@login_required
def eliminar_producto(id):
    try:
        datos = json.loads(request.cookies.get(str(current_user.id)))
    except:
        datos = []
    new_datos = []

    for dato in datos:
        if dato['id'] != id:
            new_datos.append(dato)

    resp = make_response(redirect(url_for('carro.carrito')))
    resp.set_cookie(str(current_user.id), json.dumps(new_datos))
    return resp


@carro.context_processor
@login_required
def contar_carrito():
    if not current_user.is_authenticated:
        return {'num_productos': 0}
    if request.cookies.get(str(current_user.id)) is None:
        return {'num_productos': 0}
    else:
        datos = json.loads(request.cookies.get(str(current_user.id)))
        return {'num_productos': len(datos)}


@carro.route('pedido')
@login_required
def pedido():
    try:
        datos = json.loads(request.cookies.get(str(current_user.id)))
    except:
        datos = []

    total = 0

    for producto in datos:
        total = total + (producto['precio'] * producto['cantidad'])

        Producto.query.get(producto['id']).stock -= producto['cantidad']
    
    resp = make_response(redirect(url_for('carro.agregar_carrito', total=total)))
    resp.set_cookie(str(current_user.id),"", expires=0)
    return resp

