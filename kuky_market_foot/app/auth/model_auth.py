from flask_login import UserMixin

from app import db

class Usuario(UserMixin,db.Model):
    __tablename__ = 'usuarios'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(30), nullable=False)
    password = db.Column(db.String(128), nullable=False)
    admin = db.Column(db.Boolean)


    def __init__(self, nombre, password, admin=False):
        self.nombre = nombre
        self.password = password
        self.admin = admin


    def __repr__(self):
        return f"Nombre: {self.nombre}"


    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def is_admin(self):
        return self.admin


# admin = admin = pbkdf2:sha256:260000$UFRb0GaZMu5PMPPH$002567a2fdec4141611dce7166d051293862b81f9b8d64aa3c14e97b081ee2ba
# vendedor = vendedor = pbkdf2:sha256:260000$h6lUp0JfFahkwmM1$31f23d7368881b409ac71ad83456d8c15b23f555daebc74142998f8eb17d1367
