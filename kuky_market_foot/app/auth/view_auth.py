from flask import render_template, Blueprint, request, redirect, url_for, flash
from flask_login import current_user, login_user, logout_user, login_required
from flask_bcrypt import Bcrypt

from .model_auth import Usuario
from .form_auth import RegistrarForm, LoginForm
from app import login_manager, db


bcrypt = Bcrypt()


auth = Blueprint('auth', __name__, url_prefix="/auth/")


@login_manager.user_loader
def load_user(id):
    return Usuario.query.get(int(id))


@auth.route('/registrar', methods=['GET','POST'])
def registrar():
    form = RegistrarForm()

    if form.validate_on_submit():
        if Usuario.query.filter_by(nombre=form.nombre.data).first():
            flash(f"El usuario {form.nombre.data} ya existe!")

            return redirect(url_for('auth.login'))
        else:
            hpass = bcrypt.generate_password_hash(form.password.data)
            nuevo_usuario = Usuario(nombre=form.nombre.data, password=hpass)
            db.session.add(nuevo_usuario)
            db.session.commit()

            flash("Datos ingresados correctamente!")
            return redirect(url_for('auth.login'))

    return render_template("auth/registrar.html", form=form)



@auth.route('/login', methods=['GET','POST'])
def login():

    if current_user.is_authenticated:
        return redirect(url_for("index"))

    form = LoginForm()

    if form.validate_on_submit():

        usuario = Usuario.query.filter_by(nombre=form.nombre.data).first()
        if usuario:

            if bcrypt.check_password_hash(usuario.password, form.password.data):
                flash(f"Bienvenido {usuario.nombre}", category='success')
                login_user(usuario, remember=True)

                return redirect(url_for('index'))
            else:
                flash("Datos incorrectos!!", category='error')
        else:
            flash("El usuario no existe!!", category='error')
            return redirect(url_for("auth.registrar"))

    return render_template('auth/login.html', form=form)


@auth.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("index"))