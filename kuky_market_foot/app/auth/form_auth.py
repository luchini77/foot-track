from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import InputRequired, Length, EqualTo


class RegistrarForm(FlaskForm):
    nombre = StringField('Nombre', validators=[InputRequired(), Length(min=4, max=20)])
    password = PasswordField('Contraseña', validators=[InputRequired(), Length(min=4, max=20), EqualTo('confirm')])
    confirm = PasswordField('Repite Contraseña')
    submit = SubmitField('Guardar')



class LoginForm(FlaskForm):
    nombre = StringField('Nombre', validators=[InputRequired(), Length(min=4, max=20)])
    password = PasswordField('Contraseña', validators=[InputRequired(), Length(min=4, max=20)])
    submit = SubmitField('Login')