from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_wtf.csrf import CSRFProtect
from flask_migrate import Migrate


app = Flask(__name__)

CSRFProtect(app)

#CARGAR CONFIG
app.config.from_object('config.DevelopmentConfig')


db = SQLAlchemy(app)

migrate = Migrate(app, db)

login_manager = LoginManager()
login_manager.login_view = "auth.login"
login_manager.init_app(app)


from app.general.error_handle import pagina_no_encontrada


# IMPORTAR VISTAS
from app.auth.view_auth import auth
from app.market.view_market import market
from app.market.carrito import carro


app.register_blueprint(auth)
app.register_blueprint(market)
app.register_blueprint(carro)
