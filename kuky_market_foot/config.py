class Config:
    DEBUG = True
    TESTING =True
    
    #BASE DATOS
    SQLALCHEMY_DATABASE_URI = "sqlite:////home/luchini77/Escritorio/Python-Web/practicas_mas_refinadas/en_flask/kuky_market_foot/datos/market.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    
    
class ProductionConfig(Config):
    DEBUG = False
    
    
class DevelopmentConfig(Config):
    SECRET_KEY = 'kukarra_tienda'
    ENV = 'development'